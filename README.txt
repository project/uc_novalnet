/* $Id:

- Ubercart Novalnet payment gateway integration -

This module integrates the Novalnet (www.novalnet.de) payment gateway in ubercart.
Currently this module integrates four payment methods:

- Bank debit
- Credit cart
- Bank prepay
- Sofortueberweisung.de

- Installations -

1. Copy the module folder in the modules folder of your project (e. g. /sites/all/modules)

2. Visit admin/build/modules and activate at least the Novalnet core module. All
   payment methods stand for their own so you should activate at least one payment
   method ;)
   
3. Visit /admin/store/settings/novalnet and enter your Novalnet auth code, product id, etc.


- Contact -

In case of any questions contact Niko (nikolai@kommune3.org)