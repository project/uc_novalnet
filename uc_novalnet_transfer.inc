<?php
// $Id:

/**
 * @file
 * Encoding and decoding of data to be transferred to Novalnet.
 * Based on sample code by Novalnet AG.
 */

function encode($data) {
  $key = variable_get('uc_novalnet_transfer_key', 'a87ff679a2f3e71d9181a67b7542122c');
  $data = trim($data);
  if ($data == '') return 'Error: no data';
  if (!function_exists('base64_encode') or !function_exists('pack') or !function_exists('crc32')){return 'Error: func n/a';}

  try {
    $crc = sprintf('%u', crc32($data));# %u ist obligatorisch f�r ccrc32, gibt einen vorzeichenbehafteten Wert zur�ck
    $data = $crc."|".$data;
    $data = bin2hex($data.$key);
    $data = strrev(base64_encode($data));
  } catch (Exception $e){
    echo('Error: '.$e);
  }
  return $data;
}

function decode($data) {
  $key = variable_get('uc_novalnet_transfer_key', 'a87ff679a2f3e71d9181a67b7542122c');
  $data = trim($data);
  if ($data == '') {
  	return 'Error: no data';
  }
  if (!function_exists('base64_decode') or !function_exists('pack') or !function_exists('crc32')){return 'Error: func n/a';}

  try {
    $data = base64_decode(strrev($data));
    $data = pack("H".strlen($data), $data);
    $data = substr($data, 0, stripos($data, $key));
    $pos = strpos($data, "|");
    if ($pos === FALSE){
      return("Error: CKSum not found!");
    }
    $crc = substr($data, 0, $pos);
    $value = trim(substr($data, $pos+1));
    if ($crc != sprintf('%u', crc32($value))){
      return("Error; CKSum invalid!");
    }
    return $value;
  } catch (Exception $e){
    echo('Error: '.$e);
  }
}

function hashIt($h) {
  $key = variable_get('uc_novalnet_transfer_key', 'a87ff679a2f3e71d9181a67b7542122c');
  if (!$h) return'Error: no data';
  if (!function_exists('md5')){
  	return'Error: func n/a';
  }
  if (is_array($h)){
    return md5($h['auth_code'].$h['product_id'].$h['tariff'].$h['amount'].$h['test_mode'].$h['uniqid'].strrev($key));
  }
  else {
    return md5($h.strrev($key));
  }
}

function checkHash($request) {
  $key = variable_get('uc_novalnet_transfer_key', 'a87ff679a2f3e71d9181a67b7542122c');
  if (!$request) return FALSE;
  if (is_array($request)){
    $h['auth_code'] = $request['auth_code'];
    $h['product_id'] = $request['product'];
    $h['tariff'] = $request['tariff'];
    $h['amount'] = $request['amount'];
    $h['test_mode'] = $request['test_mode'];
    $h['uniqid'] = $request['uniqid'];
  }
  if ($request['hash2'] != hashIt($h)){
    return false;
  }
  return true;
}
